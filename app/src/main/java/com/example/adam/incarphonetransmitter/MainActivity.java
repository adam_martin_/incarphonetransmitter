package com.example.adam.incarphonetransmitter;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    Button buttonCall, intent2, pickButton;
    public static final int PICK_CONTACT = 100;
    TextView textResponse, textNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonCall      = (Button)findViewById(R.id.callButton);
        pickButton      = (Button)findViewById(R.id.buttonPick);
        intent2         = (Button)findViewById(R.id.button);
        textResponse    = (TextView)findViewById(R.id.response);
        textNumber      = (TextView) findViewById(R.id.number);


        Intent x = this.getIntent();


        Uri intentUri = getIntent().getData();
        String to = URLDecoder.decode(String.valueOf(intentUri).replace("smsto:", ""));

        if ( to != "" ) {
            MyClientTask myClientTask = new MyClientTask(
                    // todo Will need to make this a configuration variable
                    "192.168.0.75",
                    8080,
                    "tel:"+to
            );

            myClientTask.execute();
        }


        buttonCall.setOnClickListener(buttonCallOnClickListener);

        intent2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent2 = new Intent();
                intent2.setAction("com.amartin.TEST");
                sendBroadcast(intent2);
            }
        });

        Log.i("Change Text", "INITfffff");

//        Toast.makeText(null, "STARTING", Toast.LENGTH_SHORT);


        pickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);  //should filter only contacts with phone numbers
                startActivityForResult(intent, PICK_CONTACT);
            }
        });
    }

    OnClickListener buttonCallOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View arg0) {
            Log.i("Change Text", "INIT");

            Intent intent = new Intent();
            intent.setAction("com.tutorialspoint.CUSTOM_INTENT");
            sendBroadcast(intent);


            MyClientTask myClientTask = new MyClientTask(
                    // todo Will need to make this a configuration variable
                    "192.168.0.75",
                    8080,
                    "tel:"+textNumber.getText().toString()
            );

            myClientTask.execute();
        }
    };


    private static final String[] phoneProjection = new String[] { ContactsContract.CommonDataKinds.Phone.DATA };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("Number", "Testing");
        super.onActivityResult(requestCode, resultCode, data);
        if (PICK_CONTACT != requestCode || RESULT_OK != resultCode){
            Toast.makeText(null, "RETURNING:" + resultCode, Toast.LENGTH_SHORT);
            return;
        }
        Uri contactUri = data.getData();
        Toast.makeText(null, "TESTING", Toast.LENGTH_SHORT);
        if (null == contactUri) return;
        //no tampering with Uri makes this to work without READ_CONTACTS permission
        Cursor cursor = getContentResolver().query(contactUri, phoneProjection, null, null, null);
        if (null == cursor) return;
        try {
            while (cursor.moveToNext()) {
                String number = cursor.getString(0);
                Log.d("Number", number);
//                Log.d(1, 'Number' + number);
                // ... use "number" as you wish
//                Toast.makeText("TEST", "Intent Detected FROM APP 1", Toast.LENGTH_LONG).show();

                MyClientTask myClientTask = new MyClientTask(
                        // todo Will need to make this a configuration variable
                        "192.168.0.75",
                        8080,
                        "tel:"+number.toString()
                );

                myClientTask.execute();
            }
        } finally {
            cursor.close();
        }
        // "cursor" is closed here already
    }


}
